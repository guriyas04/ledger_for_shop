import util
from time import sleep


def main_menu():
    util.clear()
    util.print_new_line(3)
    util.print_tab(util.CENTER_SCREEN)
    print("Welcome to main menu")
    print('')
    print("1. Setting", end="")
    util.print_tab(3)
    print("2. Purchase")
    print("3. Sale", end="")
    util.print_tab(4)
    print("4. Purchase_return")
    print("5. Sale_return", end="")
    util.print_tab(3)
    print("6. Stock_view")
    print("7. Profit_loss", end="")
    util.print_tab(3)
    print("8. Purchase_history")
    print("9. Sale_history", end="")
    util.print_tab(3)
    print("10. Logout")

    option = input('Enter your choice: ')

