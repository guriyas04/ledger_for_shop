from os import system, name

CENTER_SCREEN = 9

def clear():
    if name == 'nt':
        _ = system('cls')
    else:
        _ = system('clear')


def print_new_line(times):
    for i in range(times):
        print("\n")


def print_tab(times):
    for i in range(times):
        print("\t", end="")


def welcome_message():
    print_new_line(8)
    print_tab(CENTER_SCREEN)
    print("Welcome to Hemu Mart")

